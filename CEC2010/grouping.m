% Author: Dr. Yuan SUN
% email address: yuan.sun@unimelb.edu.au OR suiyuanpku@gmail.com
% ------------
% Description:
% ------------
% grouping - This function is used to read the rdg2 datafiles and
% form the subcomponents which are used by the cooperative co-evolutionary
% framework. This function is called in cmaescc.m.
%
%--------
% Inputs:
%--------
%    fun : the function id for which the corresponding grouping should
%          be loaded by reading the differential grouping datafiles.

function group = grouping(fun)  
    filename = sprintf('./rdg2/results/F%02d', fun);
    load(filename);
    group = nonseps;

    if(~isempty(seps))
        group = {group{1:end} seps};
    end
end
