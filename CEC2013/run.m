% Author: Dr. Yuan SUN
% email address: yuan.sun@unimelb.edu.au OR suiyuanpku@gmail.com
%
% ------------
% Description:
% ------------
% This file is the entry point for the CMAESCC-RDG2 algorithm which is a 
% cooperative co-evolutionary CMA-ES based on the recursive differential
% grouping 2 algorithm for finding the non-separable groups in black-box 
% optimization.
%
% -----------
% References:
% -----------
% Sun Y, Omidvar, M N, Kirley M, Li X. Adaptive Threshold Parameter Estimation
% with Recursive Differential Grouping for Problem Decomposition. In Proceedings 
% of the 2018 Annual Conference on Genetic and Evolutionary Computation.
% ACM, accepted March 2018.
% 
% Sun Y, Kirley M, Halgamuge S K. A Recursive Decomposition Method for Large 
% Scale  Continuous Optimization[J]. IEEE Transactions on Evolutionary Computation,
% To appear.
%
% --------
% License: 
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Yuan SUN
% e-mail: yuan.sun@unimelb.edu.au OR suiyuanpku@gmail.com
% Copyright notice: (c) 2018 Yuan Sun

clear;
% set random seed
rand('state', sum(100*clock)); 
randn('state', sum(100*clock));
%warning('off' ,'Octave:divide-by-zero');

% number of independent runs
runs = 25;

% number of fitness evaluations
Max_FEs = 3e6;

% for the benchmark functions initialization
global initial_flag;

myfunc = [1:15];
addpath('benchmark');
addpath('benchmark/datafiles');
for func_num = myfunc 
    % load the FEs used by RDG2 in the decomposition process
    decResults = sprintf('./rdg2/results/F%02d', func_num);
    load (decResults);
    FEs = Max_FEs - FEs;
    
    % set the dimensionality and upper and lower bounds of the search space
    if (ismember(func_num, [13,14]))
        D = 905;
        Lbound = -100.*ones(1,D);
        Ubound = 100.*ones(1,D);
    elseif (ismember(func_num, [1,4,7,8,11,12,15]))
        D = 1000;
        Lbound = -100.*ones(1,D);
        Ubound = 100.*ones(1,D);
    elseif (ismember(func_num, [2,5,9]))
        D=1000;
        Lbound = -5.*ones(1,D);
        Ubound = 5.*ones(1,D);
    else 
        D=1000;
        Lbound = -32.*ones(1,D);
        Ubound = 32.*ones(1,D);
    end

    VTRs = [];
    bestval = [];
    for runindex = 1:runs
        % trace the fitness
        fprintf(1, 'Function %02d, Run %02d\n', func_num, runindex);
        filename = sprintf('trace/tracef%02d_%02d.txt', func_num, runindex);
        [fid, message] = fopen(filename, 'w');
        
        initial_flag = 0;
        % call the cmaescc algorithm
        [val]  = cmaescc('benchmark_func', func_num, D, Lbound, Ubound, FEs, fid);
        bestval = [bestval; val];
        fclose(fid);
    end
    
    bestval = sort(bestval);
    % the best results of each independent run
    filename = sprintf('result/bestf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', bestval);
    fclose(fid);
    
    % mean
    filename = sprintf('result/meanf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', mean(bestval)); 
    fclose(fid);
    
    %median
    filename = sprintf('result/medianf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', median(bestval));
    fclose(fid);
    
    % std
    filename = sprintf('result/stdf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', std(bestval));
    fclose(fid);
end

